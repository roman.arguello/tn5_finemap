from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from random import randint


start = 8079469
end = 9808228
chrX_region=[]

for seq_record in SeqIO.parse("simG2B111a_cons.fasta", "fasta"):
    if (seq_record.id) == "Scf_X":
        print(seq_record.id)
        print(repr(seq_record.seq[start:end]))
        print(len(seq_record))
        SeqIO.write(seq_record[start:end], "chrX_simG2B111a_IntroReg.fasta", "fasta") 

        
for seq_record in SeqIO.parse("simG2B112a_cons.fasta", "fasta"):
    if (seq_record.id) == "Scf_X":
        print(seq_record.id)
        print(repr(seq_record.seq[start:end]))
        print(len(seq_record))
        SeqIO.write(seq_record[start:end], "chrX_simG2B112a_IntroReg.fasta", "fasta") 

        
for seq_record in SeqIO.parse("simG2B113a_cons.fasta", "fasta"):
    if (seq_record.id) == "Scf_X":
        print(seq_record.id)
        print(repr(seq_record.seq[start:end]))
        print(len(seq_record))
        SeqIO.write(seq_record[start:end], "chrX_simG2B113a_IntroReg.fasta", "fasta") 

        
for seq_record in SeqIO.parse("dsim-all-chromosome-r2.02.fasta", "fasta"):
    if (seq_record.id) == "Scf_X":
        print(seq_record.id)
        print(repr(seq_record.seq[start:end]))
        print(len(seq_record))
        SeqIO.write(seq_record[start:end], "chrX_simr202_IntroReg.fasta", "fasta") 
